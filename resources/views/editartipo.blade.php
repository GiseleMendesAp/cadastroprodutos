@extends('layout.app', ["current" => "tipos"])

@section('body')

<div class="card border">
    <div class="card-body">
    <form action="/tipos/{{$tipo->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="nometipo">Nome do Tipo</label>
                <input type="text" class="form-control" name="nomeTipo" value="{{$tipo->nome}}"
                       id="nomeTipo" placeholder="Informe o tipo que deseja cadastrar">
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Gravar alterações</button>
            <a href="/tipos" class="btn btn-sm btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
</script>
@endsection