@extends('layout.app', ["current" => "produtos"])

@section('body')

<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Cadastro de Produtos</h5>

        @if(count($prods) > 0)
        <table class="table table-ordered table-hover">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Nome do Produtos</th>
                    <th>Quantidade</th>
                    <th>Cor</th> 
                    <th>Ações</th> 
                </tr>
            </thead>
            <tbody>
            @foreach($prods as $prod)
                <tr>
                    <td>{{$prod->tipo->nome}}</td>
                    <td>{{$prod->nome}}</td>
                    <td>
                    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <form action="/produtos/remover/{{$prod->id}}" method="POST">
                                @csrf
                                @if(($prod->quantidade) == 0)
                                    <button type="submit" class="btn btn-secondary" disabled name="quantidadeProduto">-</button>
                                @else
                                    <button type="submit" class="btn btn-secondary" name="quantidadeProduto">-</button>
                                @endif
                            </form>
                        <div style="margin: .5em 1em 0 1em;">
                            {{$prod->quantidade}}
                        </div>    
                        <div>
                            <form action="/produtos/adicionar/{{$prod->id}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-secondary">+</button>
                            </form>
                        </div>
                        </div>
                    </div>
                    </td>
                    <td>
                        @if(count($prod->cores) > 0)
                            @foreach($prod->cores as $c) 
                            {{$c->nome}}  
                            @endforeach
                        @endif
                    </td>
                    <td> 
                        <a href="/produtos/editar/{{$prod->id}}" class="btn btn-sm btn-primary">Editar</a>
                        <a href="/produtos/apagar/{{$prod->id}}" class="btn btn-sm btn-danger">Apagar</a>
                    </td>
                </tr>
            @endforeach                
            </tbody>  
        </table>
        @else
        <p>Você não possui nenhum produto cadastrado!</p> 
        @endif        
    </div>
    <div class="card-footer">
        <a href="/produtos/novo" class="btn btn-sm btn-primary" role="button">+ Adicionar Produto</a>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

</script>
@endsection