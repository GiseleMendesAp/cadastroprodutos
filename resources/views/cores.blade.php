@extends('layout.app', ["current" => "cores"])

@section('body')

<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Cadastro de cores de produtos</h5>
        @if(count($cores) > 0)
        <table class="table table-ordered table-hover">
            <thead>
                <tr>
                    <th>Cores</th>
                    <th>Ações</th> 
                </tr>
            </thead>
            <tbody>
            @foreach($cores as $cor)
                <tr>
                    <td>{{$cor->nome}}</td>
                    <td> 
                        <a href="/cores/editar/{{$cor->id}}" class="btn btn-sm btn-primary">Editar</a>
                        <a href="/cores/apagar/{{$cor->id}}" class="btn btn-sm btn-danger">Apagar</a>
                    </td>
                </tr>
            @endforeach                
            </tbody>   
        </table>
        @else
        <p>Você não possui nenhuma cor cadastrada!</p>
        @endif        
    </div>
    <div class="card-footer">
        <a href="/cores/novo" class="btn btn-sm btn-primary" role="button">+ Adicionar Cor</a>
    </div>
</div>
@endsection