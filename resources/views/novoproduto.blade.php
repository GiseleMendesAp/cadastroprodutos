@extends('layout.app', ["current" => "produtos"])

@section('body')

<div class="card border">
    <div class="card-body">
        <form action="/produtos" method="POST">
            @csrf
            <div class="form-group">
                <label for="nomeProduto">Nome do Produto</label>
                <input type="text" class="form-control" name="nomeProduto" 
                       id="nomeProduto" placeholder="Produto">
            </div>
            <div class="form-group">
                <label for="tipoProduto" class="control-label">Tipo</label>
                <div class="input-group">
                    <select class="form-control" name="tipoProduto" id="tipoProduto" >
                        @foreach ($tipo as $t)
                            <option value=" {{$t->id}} "> {{$t->nome}} </option>
                        @endforeach
                    </select>    
                </div>
            </div>
            <div class="form-group">
                <label for="quantidadeProduto">Quantidade</label>
                <input type="number" class="form-control" name="quantidadeProduto" 
                       id="quantidadeProduto" placeholder="Estoque">
            </div>
            <label class="form-check-label">Cores (Selecione uma ou mais)</label>
            @foreach($cor as $key => $c)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="corProduto[]" value=" {{$c->id}}" id="corProduto{{$key}}">
                    <label class="form-check-label" for="corProduto{{$key}}"> {{$c->nome}} </label>
                </div>
            @endforeach
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <a href="/produtos" class="btn btn-sm btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    function novoProduto() {
        $('#id').val('');
        $('#nomeProduto').val('');
        $('#quantidadeProduto').val('');
        $('#dlgProdutos').modal('show');
    }


</script>
@endsection