@extends('layout.app', ["current" => "tipos"])

@section('body')

<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Cadastro de tipos de produtos</h5>
        @if(count($tipos) > 0)
        <table class="table table-ordered table-hover">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Ações</th> 
                </tr>
            </thead>
            <tbody>
            @foreach($tipos as $tipo)
                <tr>
                    <td>{{$tipo->nome}}</td>
                    <td> 
                        <a href="/tipos/editar/{{$tipo->id}}" class="btn btn-sm btn-primary">Editar</a>
                        <a href="/tipos/apagar/{{$tipo->id}}" class="btn btn-sm btn-danger">Apagar</a>
                    </td>
                </tr>
            @endforeach              
            </tbody>
        </table>
        @else
        <p>Você não possui nenhum tipo cadastrado!</p>  
        @endif        
    </div>
    <div class="card-footer">
        <a href="/tipos/novo" class="btn btn-sm btn-primary" role="button">+ Adicionar Tipo</a>
    </div>
</div>
@endsection