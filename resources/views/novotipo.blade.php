@extends('layout.app', ["current" => "tipos"])

@section('body')

<div class="card border">
    <div class="card-body">
        <form action="/tipos" method="POST">
            @csrf
            <div class="form-group">
                <label for="nomeTipo">Tipo</label>
                <input type="text" class="form-control" name="nomeTipo" id="nomeTipo" placeholder="Informe o tipo que deseja cadastrar">
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <a href="/tipos" class="btn btn-sm btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
</script>
@endsection