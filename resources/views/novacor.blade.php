@extends('layout.app', ["current" => "cores"])

@section('body')

<div class="card border">
    <div class="card-body">
        <form action="/cores" method="POST">
            @csrf
            <div class="form-group">
                <label for="nomeCor">Cor</label>
                <input type="text" class="form-control" name="nomeCor" id="nomeCor" placeholder="Informe a cor que deseja cadastrar">
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <a href="/cores" class="btn btn-sm btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
</script>
@endsection