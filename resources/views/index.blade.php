@extends('layout.app', ["current" => "home"])

@section('body')

<div class="jumbotron bg-light border border-secondary">
    <div class="row">
        <div class="col-sm">
            <div class="card-deck">
                <div class="card border border-primary">
                    <div class="card-body">
                        <h5 class="card-title">Tipos</h5>
                        <p class="card=text">
                            Ver tipos cadastrados
                        </p>
                        <a href="/tipos" class="btn btn-primary">Visualizar</a>
                    </div>
                </div>  
                <div class="card border border-primary">
                    <div class="card-body">
                        <h5 class="card-title">Cores</h5>
                        <p class="card=text">
                            Ver cores cadastradas
                        </p>
                        <a href="/cores" class="btn btn-primary">Visualizar</a>
                    </div>
                </div>  
                <div class="card border border-primary">
                    <div class="card-body">
                        <h5 class="card-title">Produtos</h5>
                        <p class="card=text">
                            Ver produtos cadastrados
                        </p>
                        <a href="/produtos" class="btn btn-primary">Visualizar</a>
                    </div>
                </div>   
            </div>
            <br>
            <div class="alert alert-primary" role="alert">
                Lembre-se de cadastrar primeiro o tipo e as cores antes de cadastrar o seu produto!
            </div>
        </div>
    </div>
</div>

@endsection