@extends('layout.app', ["current" => "produtos"])

@section('body')

<div class="card border">
    <div class="card-body">
    <form action="/produtos/{{$prod->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="nomeProduto">Nome do Produto</label>
            <input type="text" class="form-control" name="nomeProduto" value="{{$prod->nome}}"
                       id="nomeProduto" placeholder="Produto">
            </div>
            <div class="form-group">
                <label for="tipoProduto" class="control-label">Tipo</label>
                <div class="input-group">
                    <select class="form-control" name="tipoProduto" id="tipoProduto" >
                            @foreach ($tipo as $t)
                                @if ($t->id == $prod->tipo_id)
                                <option value=" {{$prod->tipo_id}} " selected> {{$prod->tipo->nome}} </option>" 
                                @else 
                                <option value=" {{$t->id}} "> {{$t->nome}} </option>
                                @endif
                            @endforeach
                    </select>    
                </div>
            </div>
            <div class="form-group">
                <label for="quantidadeProduto">Quantidade</label>
                <input type="number" class="form-control" name="quantidadeProduto" value="{{$prod->quantidade}}" id="quantidadeProduto">
            </div>
            <label class="form-check-label">Cores (Selecione uma ou mais)</label>
            @foreach($cor as $key => $c)
                <div class="form-check">
                    @if (in_array($c->id, $prodcor)) 
                    <input class="form-check-input" type="checkbox" checked name="corProduto[]" value=" {{$c->id}}" id="corProduto{{$key}} ">
                    <label class="form-check-label" for="corProduto{{$key}}"> {{$c->nome}}</label> 
                    @else
                    <input class="form-check-input" type="checkbox" name="corProduto[]" value=" {{$c->id}}" id="corProduto{{$key}} ">
                    <label class="form-check-label" for="corProduto{{$key}}"> {{$c->nome}} </label>  
                    @endif
                </div>
            @endforeach
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Gravar alterações</button>
            <a href="/produtos" class="btn btn-sm btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    function novoProduto() {
        $('#id').val('');
        $('#nomeProduto').val('');
        $('#quantidadeProduto').val('');
        $('#dlgProdutos').modal('show');
    }


</script>
@endsection