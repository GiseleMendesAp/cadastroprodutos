<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cor extends Model
{
    protected $table = 'cores'; 
    public function produtos() {
        return $this->belongsToMany('App\Models\Produto', 'prod_cores'); 
    }
}
