<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    public function produtos() {
        return $this->hasMany('App\Models\Produto'); 
    }
}
