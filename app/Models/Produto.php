<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProdCor;

class Produto extends Model
{
    public function tipo() {
        return $this->belongsTo('App\Models\Tipo'); 
    }

    public function cores() {
        return $this->belongsToMany('App\Models\Cor', 'prod_cores'); 
    }
}
