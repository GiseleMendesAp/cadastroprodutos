<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Produto;
use App\Models\Tipo;
use App\Models\Cor;
use App\Models\ProdCor;

class ProdutoControlador extends Controller
{

    public function index()
    {
        $prods = Produto::with(['tipo', 'cores'])->get();
        return view('produtos', compact('prods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo = Tipo::all(); 
        $cor = Cor::all();
        return view('novoproduto', compact(['tipo', 'cor']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prod = new Produto();
        $prod->nome = $request->input('nomeProduto');
        $prod->tipo_id = $request->input('tipoProduto');
        $prod->quantidade = $request->input('quantidadeProduto');
        if(isset($prod->nome) && isset($prod->quantidade)) {
            $prod->save();
            $prodcor = $request->input('corProduto');
            $prod->cores()->attach($prodcor);
        }
        else {
            Log::debug("Produto não foi salvo, pois o nome ou a quantidade não foram preenchidos");
        }
        return redirect('/produtos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prod= Produto::find($id);
        $tipo = Tipo::all();
        $cor = Cor::all();
        $prodcor = ProdCor::where('produto_id', $id)->get()->pluck('cor_id')->toArray(); 
        if(isset($prod)) {
            return view('editarproduto', compact(['prod', 'tipo', 'cor', 'prodcor']));
        }
        return redirect('/produtos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prod = Produto::find($id);
        if (isset($prod)) {
            $prod->nome = $request->input('nomeProduto');
            $prod->tipo_id = $request->input('tipoProduto');
            $prod->quantidade = $request->input('quantidadeProduto');
            $prod->save();
            $prod->cores()->detach();
            $prodcor = $request->input('corProduto');
            $prod->cores()->attach($prodcor);
            return redirect('/produtos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod = Produto::find($id);
        if (isset($prod)) {
            $prod->delete();
        }
        return redirect('/produtos');
    }

    public function adicionar($id)
    {
        $prod = Produto::find($id);
        $quant = $prod->quantidade;
        $quant = ++$quant;  
        $prod->quantidade = $quant;
        $prod->save();
        return redirect('/produtos');
    }

    public function remover($id)
    {
        $prod = Produto::find($id);
        $quant = $prod->quantidade;
        if ($quant > 0){
            $quant = --$quant;  
            $prod->quantidade = $quant;
            $prod->save();
        }
        else{
            Log::debug("Esse produto está zerado! Não é possível remover!");
        }
        return redirect('/produtos');
    }

}
