<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Cor;

class CorControlador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cores = Cor::all();
        return view('cores', compact('cores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('novacor'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cor = new Cor();
        $cor->nome = $request->input('nomeCor');
        if(isset($cor->nome)){
            $cor->save();
        }
        else{
            Log::debug("Cor não foi salva pois o nome não foi preenchido");
        }
        return redirect('/cores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cor = Cor::find($id);
        if(isset($cor)) {
            return view('editarcor', compact('cor'));
        }
        return redirect('/cores');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cor = Cor::find($id);
        if (isset($cor)) {
            $cor->nome = $request->input('nomeCor');
            $cor->save();
        }
        return redirect('/cores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cor = Cor::find($id);
        if (isset($cor)) {
            $cor->delete();
        }
        return redirect('/cores');
    }
}
