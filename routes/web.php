<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdutoControlador;
use App\Http\Controllers\TipoControlador;
use App\Http\Controllers\CorControlador;

Route::get('/', function () {
    return view('index');
});

Route::get('/produtos', [ProdutoControlador::class, 'index']);
Route::get('/produtos/novo', [ProdutoControlador::class, 'create']);
Route::post('/produtos', [ProdutoControlador::class, 'store']);
Route::post('/produtos/adicionar/{id}', [ProdutoControlador::class, 'adicionar']);
Route::post('/produtos/remover/{id}', [ProdutoControlador::class, 'remover']);
Route::get('/produtos/apagar/{id}', [ProdutoControlador::class, 'destroy']);
Route::get('/produtos/editar/{id}', [ProdutoControlador::class, 'edit']);
Route::post('/produtos/{id}', [ProdutoControlador::class, 'update']);

Route::get('/tipos', [TipoControlador::class, 'index']);
Route::get('/tipos/novo', [TipoControlador::class, 'create']);
Route::post('/tipos', [TipoControlador::class, 'store']);
Route::get('/tipos/apagar/{id}', [TipoControlador::class, 'destroy']);
Route::get('/tipos/editar/{id}', [TipoControlador::class, 'edit']);
Route::post('/tipos/{id}', [TipoControlador::class, 'update']);

Route::get('/cores', [CorControlador::class, 'index']);
Route::get('/cores/novo', [CorControlador::class, 'create']);
Route::post('/cores', [CorControlador::class, 'store']);
Route::get('/cores/apagar/{id}', [CorControlador::class, 'destroy']);
Route::get('/cores/editar/{id}', [CorControlador::class, 'edit']);
Route::post('/cores/{id}', [CorControlador::class, 'update']);



