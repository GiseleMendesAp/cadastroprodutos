<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB; 

class CorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cores')->insert(['nome' => 'Preto']);
        DB::table('cores')->insert(['nome' => 'Branco']);
        DB::table('cores')->insert(['nome' => 'Nude']);
        DB::table('cores')->insert(['nome' => 'Verde']);
        DB::table('cores')->insert(['nome' => 'Azul']);
        DB::table('cores')->insert(['nome' => 'Vermelho']);
        DB::table('cores')->insert(['nome' => 'Cinza']);
        DB::table('cores')->insert(['nome' => 'Prata']);
        DB::table('cores')->insert(['nome' => 'Dourado']);
    }
}
