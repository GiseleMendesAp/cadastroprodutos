<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos')->insert(['nome' => 'Mercadoria Revenda']);
        DB::table('tipos')->insert(['nome' => 'Produto acabado']);
        DB::table('tipos')->insert(['nome' => 'Materia prima']);
    }
}
