<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProdCorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prod_cores')->insert(['produto_id' => 1, 'cor_id' => 3]);
        DB::table('prod_cores')->insert(['produto_id' => 2, 'cor_id' => 1]);
        DB::table('prod_cores')->insert(['produto_id' => 3, 'cor_id' => 2]);
        DB::table('prod_cores')->insert(['produto_id' => 4, 'cor_id' => 7]);
        DB::table('prod_cores')->insert(['produto_id' => 5, 'cor_id' => 1]);
        DB::table('prod_cores')->insert(['produto_id' => 6, 'cor_id' => 8]);
        DB::table('prod_cores')->insert(['produto_id' => 7, 'cor_id' => 9]);
    }
}
