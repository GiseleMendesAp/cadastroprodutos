<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert(['nome' => 'Blusa', 'quantidade' => 10, 'tipo_id' => 2]);
        DB::table('produtos')->insert(['nome' => 'Calça', 'quantidade' => 5, 'tipo_id' => 2]);
        DB::table('produtos')->insert(['nome' => 'Camisa', 'quantidade' => 7, 'tipo_id' => 2]);
        DB::table('produtos')->insert(['nome' => 'Tecido', 'quantidade' =>15, 'tipo_id' => 1]);
        DB::table('produtos')->insert(['nome' => 'Linha', 'quantidade' => 12, 'tipo_id' => 1]);
        DB::table('produtos')->insert(['nome' => 'Colar', 'quantidade' => 4, 'tipo_id' => 3]);
        DB::table('produtos')->insert(['nome' => 'Pulseira', 'quantidade' => 6, 'tipo_id' => 3]);
    }
}
